const path = require('path');

const gulpConfig = {
  templateEngine: 'nunjucks', // null or nunjucks for the moment : // TODO: Add Pug and others
  templateEngineConfig: {
      path: 'src/',
      ext: '.html',
      data: require(path.join(__dirname, 'src/data/data.json')),
      inheritExtension: false,
      envOptions: {
          watch: false
      },
      manageEnv: null,
      loaders: null
  }
};

module.exports = gulpConfig;