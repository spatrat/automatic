const gulp = require('gulp');
const config = require('./gulp-config');
const requireDir = require('require-dir');
requireDir('./gulp-tasks', { recurse: true });