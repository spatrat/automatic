const gulp = require('gulp');
const config = require('../gulp-config');
const tap = require('gulp-tap');
const concat = require('gulp-concat');
const insert = require('gulp-insert');
const htmlbeautify = require('gulp-html-beautify');
const frontMatter = require('gulp-front-matter');
const gulpSort = require('gulp-sort');
const nunjucksRender = require('gulp-nunjucks-render');
const gulpif = require('gulp-if');
const replace = require('gulp-replace');
const fs = require('fs');

const templateEngine = config.templateEngine;

const menu = [];
const modulesListJson = menu;
const numModules = menu.length;

const createMenu = function(menuArray) {

    const menuItems =  menuArray.map(function(item) {

        return '<li class="sg-menu__list__item">' +
            '\n<a class="sg-menu__link" href="'+item.id+'">'+item.title+'</a>'+
            '\n</li>';
    }).join('');

    return `
<div class="sg-menu-wrapper">
    <div class="sg-menu-wrapper__inner">
        <h2 class="sg-menu__title">Modules <span class="modules-count">(%%numModules%%)</span></h2>
        <nav class="sg-menu">
            <ul class="sg-menu__list">
                ${menuItems}
            </ul>
        </nav>
    </div>
</div>`;
};

const sgModules = function() {

    const styleGuideTemplatePath = './src/styleguide/modules.html';
    const modulePageTempletStart = `
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/sg-styles.css">
    <title>Styleguide</title>
</head>
<body>
  <div class="sg-modules-page">
    <div class="sg-modules-page__inner">
    %%menu_placeholder%%
    <div class="sg-modules-list">
`;

    const modulePageTemplateEnd = `\n</div>\n</div>\n</div>
<script src="../js/sg-scripts.js"></script>
<script type="text/javascript" src="../../js/index.bundle.js"></script>
</body>
</html>`;

    return gulp.src('./src/modules/**/*.html')
        .pipe(gulpSort())
        .pipe(frontMatter({ // optional configuration
            property: 'frontMatter', // property added to file object
                                     // also works with deep property selectors
                                     // e.g., 'data.foo.bar'
            remove: true // should we remove front-matter header?
        }))
        .pipe(tap(function(file, t) {
            const contents = file.contents.toString();
            const id = file.frontMatter.title ? file.frontMatter.title.toLowerCase().replace(/ /g, '-')+'-'+ new Date().getUTCMilliseconds() : 'unnamed-module'+'-'+ new Date().getUTCMilliseconds();
            const tags = file.frontMatter.tags ? file.frontMatter.tags : '';
            const title = file.frontMatter.title ? file.frontMatter.title : 'unnamed module';
            const template = `
        <div class="sgm" id="${id}" data-tags="${tags}" data-menu-id="${id}">
            <div class="sgm__inner">
                <h3 class="sgm__title">${title}</h3>
                $$moduleContent$$
            </div>
        </div>`;

            menu.push({id: '#'+id, title:title});

            const newContents = template.replace('$$moduleContent$$', contents);
            file.contents = new Buffer(newContents);

            return file;

        }))
        .pipe(gulpif(templateEngine === 'nunjucks',
            // If nunjucks is detected
            nunjucksRender(config.templateEngineConfig)))
        .pipe(concat('modules.html'))
        .pipe(insert.wrap(modulePageTempletStart, modulePageTemplateEnd))
        .pipe(tap(function(file,t) {
            const contents = file.contents.toString();
            const newContents = contents.replace('%%menu_placeholder%%',createMenu(menu))
                .replace('%%numModules%%', menu.length.toString());
            file.contents = new Buffer(newContents);
            return file;
        }))
        .pipe(htmlbeautify({indent_char: ' ', indentSize: 4}))
        .pipe(replace(/(---(.|\n)*?---)/gm, ''))
        .pipe(gulp.dest('dist/styleguide/modules/'));

};

gulp.task('sg:modules', function() {
    sgModules();
});

gulp.task('sg:copy-js', function() {
    return gulp.src('./src/styleguide/sg-scripts.js')
        .pipe(gulp.dest('./dist/styleguide/js/'));
});

gulp.task('sg:copy-css', function() {
    return gulp.src('./src/styleguide/sg-styles.css')
        .pipe(gulp.dest('./dist/styleguide/css/'));
});

gulp.task('sg', ['sg:modules', 'sg:copy-js', 'sg:copy-css'], function() {
    console.log('[     ]', 'Creating styleguide modules json file!');
    const jsonFilePath = './src/data/modules.json';
    fs.writeFile(jsonFilePath, JSON.stringify(modulesListJson, null, 4), (err) => {
        if (err) throw err;
        console.log('The module file file has been saved in ' + jsonFilePath + '!');
    });
});