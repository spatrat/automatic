const gulp = require('gulp');
const webpack = require('webpack');
const webpackStream = require('webpack-stream');
const webpackConfig = require('../webpack.config.js');
const browserSync = require('browser-sync');


var jsTask = function() {
    console.log('Starting javascript task!');
    return gulp.src('src/js/*.js')
        .pipe(webpackStream(webpackConfig), webpack)
        .pipe(gulp.dest('dist/js'))
        .pipe(browserSync.stream());
};


gulp.task('js', function() {
    return jsTask();
});

module.exports = jsTask;
